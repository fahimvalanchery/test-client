import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/http/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  userInfo:any = null;
  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.getUserData();
  }

  getUserData() {
    this.userService.getUserIPInfo().subscribe((res:any) => {
      console.log(res);
      this.userInfo = res;
    });
  }
}
