import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  apiUrl = environment.apiUrl;
  constructor(private _http: HttpClient,) { }


  getCryptoGraphData(){
    return this._http.get(`${this.apiUrl}/dashboard/graph/crypto`)
  }
  getNonCryptoGraphData(){
    return this._http.get(`${this.apiUrl}/dashboard/graph/noncrypto`)
  }
}
