import { Component, OnInit } from '@angular/core';
import { ChartConfiguration, ChartData, ChartType } from 'chart.js';
import DataLabelsPlugin from 'chartjs-plugin-datalabels';
import { DashboardService } from 'src/app/core/http/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public barChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {},
      y: {
        min: 10
      }
    },
    plugins: {
      legend: {
        display: true,
      },
      datalabels: {
        anchor: 'end',
        align: 'end'
      }
    }
  };
  public barChartType: ChartType = 'bar';
  public barChartPlugins = [
    DataLabelsPlugin
  ];

  public cryptobarChartData: ChartData<'bar'> = {
    labels: [ ],
    datasets: [
      { data: [ ], label: '' },
    ]
  };
  public noncryptobarChartData: ChartData<'bar'> = {
    labels: [ ],
    datasets: [
      { data: [ ], label: '' },
    ]
  };

  constructor(private dashboardService:DashboardService) { }

  ngOnInit(): void {
    this.cryptoGraphdata()
    this.noncryptoGraphdata()
  }

  cryptoGraphdata(){
    this.dashboardService.getCryptoGraphData().subscribe((res:any)=>{
      this.cryptobarChartData=res
    })
  }
  noncryptoGraphdata(){
    this.dashboardService.getNonCryptoGraphData().subscribe((res:any)=>{
      this.noncryptobarChartData=res
    })
  }

}
