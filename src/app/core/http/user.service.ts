import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiUrl = environment.apiUrl;
  constructor(private _http: HttpClient,) { }


  getUserIPInfo(){
    return this._http.get(`${this.apiUrl}/user/ip`)
  }

}
